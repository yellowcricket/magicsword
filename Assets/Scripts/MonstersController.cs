﻿using UnityEngine;

public class MonstersController : MonoBehaviour {

    public enum GeneratePattern { Normal, OneToCenter, FourToCenter, ThreeRowToCenter} 
    [SerializeField] SlimeGeneric _slimeGenericPrefab;
    [SerializeField] Mushroom _mushroomPrefab;
    [SerializeField] TextMesh _debugData;
    [SerializeField] BackgroundController _bgController;

    [SerializeField] int _monsterWaveTime;
    [SerializeField] float _slimeBaseSpeed;
    [SerializeField] float _slimeSpeedIncreasePerLevelSet;
    [SerializeField] int _slimeSpeedMaximum;

    [SerializeField] int _slimeBaseHP;
    [SerializeField] int _slimeHPIncreasePerLevelSet;
    [SerializeField] int _slimeHPIncreasePerColorChange;
    [SerializeField] int _slimeHPIncreasePerSize = 25;

    [SerializeField] float _slimeSpawnBaseTime;
    [SerializeField] float _slimeSpawnTimeDecreasedPerLevelSet;
    [SerializeField] float _slimeSpawnTimeMinimum;

    [SerializeField] int _slimeSpawnCopiesMaximum;
    [SerializeField] int _startGameOnLevel = 0;
    [SerializeField] int _startWaveOn = 0;

    private int upgradeEvery4Level = 4;
    private int mushroomAtWave = 0;

    private float currentWaveTime;
    private float LastMonsterGenerationTime;

    public static float ChargeBarCenterX;
    public static float ChargeBarCenterY;

    // Use this for initialization
    void Start () {
        //Initialize();

    }

    public void Initialize()
    {
        if (_startGameOnLevel > 0)
        {
            Managers.Mission.curLevel = _startGameOnLevel;
        }
        if (_startWaveOn > 0)
        {
            Managers.Mission.curWave = _startWaveOn;
            Debug.Log("wave forced to" + _startWaveOn);
        }
        _bgController.TransitionBackground();
        LastMonsterGenerationTime = Time.time - _slimeSpawnBaseTime;
        currentWaveTime = Time.time;
        //Managers.Mission.curWave = 1;

        if (SlimeGeneric.slimeColorsList.Count == 0)
        {
            SlimeGeneric.slimeColorsList.Add(SlimeGeneric.SlimeColor.Green);
            SlimeGeneric.slimeColorsList.Add(SlimeGeneric.SlimeColor.Pink);
            SlimeGeneric.slimeColorsList.Add(SlimeGeneric.SlimeColor.Blue);
            SlimeGeneric.slimeColorsList.Add(SlimeGeneric.SlimeColor.Yellow);
            SlimeGeneric.slimeColorsList.Add(SlimeGeneric.SlimeColor.Fifth);
            SlimeGeneric.slimeColorsList.Add(SlimeGeneric.SlimeColor.Sixth);
            SlimeGeneric.slimeColorsList.Add(SlimeGeneric.SlimeColor.Seventh);
            SlimeGeneric.slimeColorsList.Add(SlimeGeneric.SlimeColor.Eighth);
            SlimeGeneric.slimeColorsList.Add(SlimeGeneric.SlimeColor.Ninth);
            SlimeGeneric.slimeColorsList.Add(SlimeGeneric.SlimeColor.Tenth);
            SlimeGeneric.slimeColorsList.Add(SlimeGeneric.SlimeColor.Eleventh);
            SlimeGeneric.slimeColorsList.Add(SlimeGeneric.SlimeColor.Twlefth);
        }


    }
	
	// Update is called once per frame
	void Update () {
        _debugData.text = "Level:" + Managers.Mission.curLevel + " Wave:" + Managers.Mission.curWave + " Time:" + (int)(Time.time - currentWaveTime);

        float spawnTimeAdjustment = 0;
        if (Managers.Mission.curLevel > 2)
        {
            spawnTimeAdjustment = (Managers.Mission.curLevel / SlimeGeneric.slimeColorsList.Count) * (_slimeSpawnTimeDecreasedPerLevelSet);
        }
        float spawnTime = _slimeSpawnBaseTime - spawnTimeAdjustment;
        if (spawnTime < _slimeSpawnTimeMinimum)
        {
            spawnTime = _slimeSpawnTimeMinimum;
        }
        if (Time.time - LastMonsterGenerationTime > (_slimeSpawnBaseTime - spawnTimeAdjustment))
        {
            GenerateMonsters();
            LastMonsterGenerationTime = Time.time;
        }
        
        if (Time.time - currentWaveTime  >= _monsterWaveTime)
        {
            Managers.Mission.IncementWave();
            mushroomAtWave--;
            if (mushroomAtWave <= 0)
            {
                mushroomAtWave += Random.Range(4, 6);
                GenerateMushroom();
            }
            _bgController.TransitionBackground();
            currentWaveTime = Time.time;
        }
	}

    private void GenerateMushroom()
    {
        Mushroom newMushroom = Instantiate(_mushroomPrefab);
        newMushroom.transform.SetParent(_mushroomPrefab.transform.parent);
        newMushroom.gameObject.SetActive(true);
        newMushroom.transform.localScale = new Vector2(5, 5);
        newMushroom.transform.position = new Vector3(Random.Range(-5f, 5f), 13, 0);
    }

    private void GenerateMonsters()
    {
        GeneratePattern Pattern = (GeneratePattern)Random.Range(0, 3);

        int levelToGenerateFor = Managers.Mission.curLevel;
        while (levelToGenerateFor > SlimeGeneric.slimeColorsList.Count)
        {
            levelToGenerateFor -= SlimeGeneric.slimeColorsList.Count;
        }
        Debug.Log("Generating for level  " + levelToGenerateFor);
        Debug.Log("Slime color count is " + SlimeGeneric.slimeColorsList.Count);
        int floorRange = levelToGenerateFor - 5;
        if (floorRange < 0)
        {
            floorRange = 0;
        }
        int whichMonster = Random.Range(floorRange, levelToGenerateFor);
        //whichMonster = 0;

        /*int HPAdjustment = 0;

        if (Managers.Mission.curLevel > 1) {
            HPAdjustment = (1 + ((Managers.Mission.curLevel - 1) / upgradeEvery4Level))  * _slimeHPIncreasePerUpgrade;
        }
        float speedAdjustment = 0;
        if (Managers.Mission.curLevel > 2)
        {
            speedAdjustment = (1 + ((Managers.Mission.curLevel - 2) / upgradeEvery4Level)) * (_slimeSpeedIncreasePerUpgrade);
        }

        if (speedAdjustment > _slimeSpeedIncreaseCap)
        {
            speedAdjustment = _slimeSpeedIncreaseCap;
        }*/
        if (Pattern == GeneratePattern.Normal)
        {
            int copyUpToWhichSlime = Managers.Mission.curLevel / SlimeGeneric.slimeColorsList.Count;
            copyUpToWhichSlime = copyUpToWhichSlime % SlimeGeneric.slimeColorsList.Count;
            int copies = Managers.Mission.curLevel / (SlimeGeneric.slimeColorsList.Count * SlimeGeneric.slimeColorsList.Count);//how many copies for ALL colors
                                                                                                                               //copies = copies % SlimeGeneric.slimeColorsList.Count;
            Debug.Log("copies is " + copies);
            if (copyUpToWhichSlime > whichMonster)
            {
                copies++;//if another copy is needed for this one particular color
            }
            Debug.Log("up to which slime is " + copyUpToWhichSlime);
            Debug.Log("whichMonster is " + whichMonster);
            do
            {
                SlimeGeneric newSlime = Instantiate(_slimeGenericPrefab);
                newSlime.transform.SetParent(_slimeGenericPrefab.transform.parent);
                newSlime.gameObject.SetActive(true);
                newSlime.InitializeVariables();
                newSlime.SetLevelVariables(Managers.Mission.curWave, Managers.Mission.curLevel, _slimeBaseHP, _slimeHPIncreasePerLevelSet, _slimeHPIncreasePerColorChange, _slimeHPIncreasePerSize);
                newSlime.SetUpSpawning(0, 0, transform.position, SlimeGeneric.slimeColorsList[whichMonster]);
                newSlime.transform.localPosition = new Vector2(Random.Range(-5f, 5f), 14f);
                newSlime.transform.localScale = new Vector2(newSlime.transform.localScale.x, newSlime.transform.localScale.y / newSlime.transform.parent.localScale.y);
                newSlime.GetComponent<BoxCollider2D>().enabled = true;
                copies--;
            } while (copies >= 0);
        }
        else if (Pattern == GeneratePattern.OneToCenter)
        {
            
            SlimeGeneric newSlime = Instantiate(_slimeGenericPrefab);
            newSlime.transform.SetParent(_slimeGenericPrefab.transform.parent);
            newSlime.gameObject.SetActive(true);
            newSlime.InitializeVariables();
            newSlime.SetSlimeMovement(SlimeGeneric.MovementType.Center);
            int wave = Managers.Mission.curWave % 3;//force tiniest
            if (wave == 0)
            {
                wave = 3;
            }
            newSlime.SetLevelVariables(wave, Managers.Mission.curLevel, _slimeBaseHP, _slimeHPIncreasePerLevelSet, _slimeHPIncreasePerColorChange, _slimeHPIncreasePerSize);
            newSlime.SetUpSpawning(0, 0, transform.position, SlimeGeneric.slimeColorsList[whichMonster]);
            int corner = Random.Range(0, 4);
            if (corner == 0)
            {
                newSlime.transform.localPosition = new Vector2(-8.5f, 11f);
            }
            else if (corner == 1)
            {
                newSlime.transform.localPosition = new Vector2(8.5f, 11f);
            }
            else if (corner == 2)
            {
                newSlime.transform.localPosition = new Vector2(-8.5f, 2f);
            }
            else if (corner == 3)
            {
                newSlime.transform.localPosition = new Vector2(8.5f, 2f);
            }
            newSlime.transform.localScale = new Vector2(newSlime.transform.localScale.x, newSlime.transform.localScale.y / newSlime.transform.parent.localScale.y);
            newSlime.GetComponent<BoxCollider2D>().enabled = true;
        }
        else if (Pattern == GeneratePattern.FourToCenter)
        {
            for (int corner = 0; corner< 4; corner++)
            {
                SlimeGeneric newSlime = Instantiate(_slimeGenericPrefab);
                newSlime.transform.SetParent(_slimeGenericPrefab.transform.parent);
                newSlime.gameObject.SetActive(true);
                newSlime.InitializeVariables();
                newSlime.SetSlimeMovement(SlimeGeneric.MovementType.Center);
                int wave = Managers.Mission.curWave % 3;//force tiniest
                if (wave == 0)
                {
                    wave = 3;
                }
                newSlime.SetLevelVariables(wave, Managers.Mission.curLevel, _slimeBaseHP, _slimeHPIncreasePerLevelSet, _slimeHPIncreasePerColorChange, _slimeHPIncreasePerSize);
                newSlime.SetUpSpawning(0, 0, transform.position, SlimeGeneric.slimeColorsList[whichMonster]);

                if (corner == 0)
                {
                    newSlime.transform.localPosition = new Vector2(-8.5f, 11f);
                }
                else if (corner == 1)
                {
                    newSlime.transform.localPosition = new Vector2(8.5f, 11f);
                }
                else if (corner == 2)
                {
                    newSlime.transform.localPosition = new Vector2(-8.5f, 2f);
                }
                else if (corner == 3)
                {
                    newSlime.transform.localPosition = new Vector2(8.5f, 2f);
                }
                newSlime.transform.localScale = new Vector2(newSlime.transform.localScale.x, newSlime.transform.localScale.y / newSlime.transform.parent.localScale.y);
                newSlime.GetComponent<BoxCollider2D>().enabled = true;
            }
        }


    }
}

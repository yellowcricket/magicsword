﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeAttack : MonoBehaviour {
    [SerializeField] GrenadeExplosion _explosionPrefab;
    float velocity = -10;
    float distanceToGo;
    // Use this for initialization
    void Start () {
		
	}

    public void SetDistanceToGo(float distance)
    {
        distanceToGo = distance;
        if (distanceToGo > 18)
        {
            distanceToGo = 18;
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.localPosition -= new Vector3(0, velocity * Time.deltaTime, 0);
        transform.localEulerAngles += new Vector3(0, 0, Time.deltaTime * 500);
        if (transform.localPosition.y  + 10 >  distanceToGo)
        {
            GrenadeExplosion newExplosion = Instantiate(_explosionPrefab);
            newExplosion.gameObject.SetActive(true);
            newExplosion.transform.position = transform.position;
            newExplosion.transform.localScale = new Vector2(Managers.Character.GetGrenadeRadius(), Managers.Character.GetGrenadeRadius());
            
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        /*Debug.Log("something hit");
        if (collision.collider.GetComponent<Enemy>() != null)
        {
            Debug.Log("green hit");
            collision.collider.GetComponent<Enemy>().OnHit();
            Destroy(gameObject);
        }*/
    }
}

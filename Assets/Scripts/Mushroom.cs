﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mushroom : MonoBehaviour {
    public bool MushroomObtained { get; private set; }
	// Use this for initialization
	void Start () {
        MushroomObtained = false;
	}
	
	// Update is called once per frame
	void Update () {
        transform.localPosition += new Vector3(0, -2 * Time.deltaTime, 0);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("something hit mushroom");
        if (collision.GetComponent<Hero>() != null)
        {
            MushroomObtained = true;
        }
        else if (collision.GetComponent<Enemy>() != null)
        {
            Physics2D.IgnoreCollision(GetComponent<BoxCollider2D>(), collision.GetComponent<BoxCollider2D>());
        }
        else if (collision.GetComponent<SwordAttack>())
        {

            Destroy(gameObject);
        }

    }
}

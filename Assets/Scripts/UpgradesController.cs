﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradesController : MonoBehaviour {
    [SerializeField] Text _coinsText;

    [SerializeField] Button _swordAttackButton;
    [SerializeField] Button _grenadeDamageButton;
    // Use this for initialization
    void Start () {
        UpdateUI();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnSwordAttackPress()
    {
        int upgradeCost = Managers.Character.GetSwordAttackCost();
        if (Managers.Character.CoinsObtained > upgradeCost)
        {
            Managers.Character.UpgradeSwordAttack();
            
        }
        UpdateUI();
    }

    public void OnGrenadePress()
    {
        int upgradeCost = Managers.Character.GetGrenadeCost();
        if (Managers.Character.CoinsObtained > upgradeCost)
        {
            Managers.Character.UpgradeGrenadeLevel();

        }
        UpdateUI();
    }

    public void UpdateUI()
    {
        _coinsText.text = Managers.Character.CoinsObtained.ToString();
        _swordAttackButton.transform.Find("Level").GetComponent<Text>().text = "Level " + Managers.Character.SwordAttackLevel;
        _swordAttackButton.transform.Find("Cost").GetComponent<Text>().text = Managers.Character.GetSwordAttackCost() + " coins";

        _grenadeDamageButton.transform.Find("Level").GetComponent<Text>().text = "Level " + Managers.Character.GrenadeLevel;
        _grenadeDamageButton.transform.Find("Cost").GetComponent<Text>().text = Managers.Character.GetGrenadeCost() + " coins";
    }
}

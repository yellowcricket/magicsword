﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeGeneric : Enemy {
    public enum MovementType { Pending, Straight, Diagonal, ZigZag, Center };
    public enum SlimeColor { Green, Pink, Blue, Yellow, Fifth, Sixth, Seventh, Eighth, Ninth, Tenth, Eleventh, Twlefth }
    public static List<SlimeColor> slimeColorsList = new List<SlimeColor>();
    [SerializeField] SlimeColor SlimesColor;
    [SerializeField] MovementType SlimesMovement = MovementType.Straight;
    //[SerializeField] SlimeGeneric _genericSlimePrefab;
    /*[SerializeField] RuntimeAnimatorController _greenSlimeAnim;
    [SerializeField] RuntimeAnimatorController _pinkSlimeAnim;
    [SerializeField] RuntimeAnimatorController _blueSlimeAnim;
    [SerializeField] RuntimeAnimatorController _yellowSlimeAnim;
    [SerializeField] RuntimeAnimatorController _fourthSlimeAnim;
    [SerializeField] RuntimeAnimatorController _fifthSlimeAnim;
    [SerializeField] RuntimeAnimatorController _sixthSlimeAnim;
    [SerializeField] RuntimeAnimatorController _seventhSlimeAnim;
    [SerializeField] RuntimeAnimatorController _eighthSlimeAnim;
    [SerializeField] RuntimeAnimatorController _ninthSlimeAnim;
    [SerializeField] RuntimeAnimatorController _tenthSlimeAnim;
    [SerializeField] RuntimeAnimatorController _eleventhSlimeAnim;
    [SerializeField] RuntimeAnimatorController _twlefthSlimeAnim;*/

    [SerializeField] DamageFloater _damageFloaterPrefab;

    //used for dynamic updating of sprites
    private string resourcePathSlimeName = "slime_green";
    private float speedAdjustment;

    Animator animator;
    
    bool DirectionNeedsUpdating = true;
    bool DirectionUpdated = false;
    float xDir = 0;

    //bool IsBig = true;
    bool Spawning = false;
    public bool Dying { get; private set; }
    int DirectionH = 0;
    int DirectionV = 0;
    private int LevelCreated;
    private int WaveCreated;
    private int HPLevelUpgradeIncrease;
    private int HPColorUpgradeIncrease;
    private int HPSizeUpgradeIncrease;
    private int HPBase;


    public void InitializeVariables()
    {
        Dying = false;

    }
    // Use this for initialization
    void Start()
    {
        InitializeVariables();
        animator = GetComponent<Animator>();

        //GetComponent<Animator>().state
    }
    private void LateUpdate()
    {
        GetComponent<SpriteRenderer>().sortingOrder = Mathf.RoundToInt(transform.position.y * 100f) * -1;
    }
    // Update is called once per frame
    void Update()
    {

        if (SlimesMovement == MovementType.ZigZag)
        {
            if (transform.localPosition.x <= -5)
            {
                transform.localPosition = new Vector2(-5, transform.localPosition.y);
                xDir = Random.Range(0, 4);

            }
            else if (transform.localPosition.x >= 5)
            {
                transform.localPosition = new Vector2(5, transform.localPosition.y);
                xDir = Random.Range(-4, 0);
            } else if (DirectionNeedsUpdating)
            {
                DirectionNeedsUpdating = false;
                DirectionUpdated = true;
                xDir = Random.Range(-4, 4);
                
                /*else if (transform.localPosition.x + xDir < 5)
                {
                    xDir = -5 - transform.localPosition.x;
                }
                else if (transform.localPosition.x + xDir > 5)
                {
                    xDir = 5 - transform.localPosition.x;
                }*/
            }
        }
        else if (SlimesMovement == MovementType.Diagonal)
        {
            if (xDir > 0 && transform.position.x > 5)
            {
                xDir = -2;
            }
            else if (xDir < 0 && transform.position.x < -5)
            {
                xDir = 2;
            }

        } else if (SlimesMovement == MovementType.Center)
        {
            Debug.Log("Charge bar center is X:" + MonstersController.ChargeBarCenterX + " Y:" + MonstersController.ChargeBarCenterY); 
            float centerOfSlimeX = GetComponent<SpriteRenderer>().sprite.bounds.size.x / 2;
            float curDistanceFromBottomY = transform.position.y - MonstersController.ChargeBarCenterY;
            float curDistanceFromCenterX = transform.position.x;
            float ratioY = -speedAdjustment / curDistanceFromBottomY;
            xDir = curDistanceFromCenterX * ratioY;

        }

        if (animator.GetBool("Grounded") && !DirectionUpdated)
        {
            DirectionNeedsUpdating = true;
        }


        //Spawning related stuff
        if (animator.GetBool("FinishedInitialJump"))
        {
            Spawning = false;
        }

        if (Spawning)
        {
            transform.localPosition -= new Vector3(DirectionH * Time.deltaTime, DirectionV * Time.deltaTime, 0);
        }
        else if (!animator.GetBool("Grounded") && !Dying)
        {
            transform.localPosition += new Vector3(xDir * Time.deltaTime, -speedAdjustment * Time.deltaTime, 0);
            DirectionUpdated = false;
        }

        
    }

    private IEnumerator DestroyMe()
    {
        yield return new WaitForSeconds(3);
        Destroy(gameObject);
    }

    public void Die()
    {
        GetComponent<ReskinAnimation>().UpdateSpritePath("Slimes/" + resourcePathSlimeName + "/death");
        GetComponent<ReskinAnimation>().replaceImages();
        animator.SetBool("Dying", true);
        Dying = true;
        StartCoroutine(DestroyMe());
        Managers.Audio.PlaySlimeDie();
        //if (SlimesColor != SlimeColor.Green && SlimesColor != SlimeColor.Fifth && SlimesColor != SlimeColor.Ninth)
        if (WaveCreated > 3)
        {
            SlimeGeneric babySlime1 = Instantiate(gameObject).GetComponent<SlimeGeneric>();
            babySlime1.gameObject.SetActive(true);
            babySlime1.transform.SetParent(gameObject.transform.parent);
            SlimeGeneric babySlime2 = Instantiate(gameObject).GetComponent<SlimeGeneric>();
            babySlime2.gameObject.SetActive(true);
            babySlime2.transform.SetParent(gameObject.transform.parent);
            SlimeColor colorToMake = slimeColorsList[slimeColorsList.IndexOf(SlimesColor)];
            babySlime1.SetLevelVariables(WaveCreated - 3,LevelCreated,HPBase, HPLevelUpgradeIncrease, HPColorUpgradeIncrease, HPSizeUpgradeIncrease);
            babySlime1.SetUpSpawning(-2, 0, transform.position, colorToMake);
            babySlime2.SetLevelVariables(WaveCreated - 3, LevelCreated, HPBase, HPLevelUpgradeIncrease, HPColorUpgradeIncrease, HPSizeUpgradeIncrease);
            babySlime2.SetUpSpawning(2, 0, transform.position, colorToMake);
            //if (slimeColorsList.IndexOf(SlimesColor) % 4 == 2)
            if (WaveCreated > 6)
            {
                SlimeGeneric babySlime3 = Instantiate(gameObject).GetComponent<SlimeGeneric>();
                babySlime3.gameObject.SetActive(true);
                babySlime3.SetLevelVariables(WaveCreated - 3, LevelCreated, HPBase, HPLevelUpgradeIncrease, HPColorUpgradeIncrease, HPSizeUpgradeIncrease);
                babySlime3.SetUpSpawning(0, 2, transform.position, colorToMake);
                babySlime3.transform.SetParent(gameObject.transform.parent);
            }
            //if (slimeColorsList.IndexOf(SlimesColor) % 4 == 3)
            /*if (WaveCreated > 9)
            {
                SlimeGeneric babySlime4 = Instantiate(gameObject).GetComponent<SlimeGeneric>();
                babySlime4.gameObject.SetActive(true);
                babySlime4.SetLevelVariables(WaveCreated - 3, LevelCreated, HPBase, HPLevelUpgradeIncrease, HPColorUpgradeIncrease);
                babySlime4.SetUpSpawning(0, -1, transform.position, colorToMake);
                babySlime4.transform.SetParent(gameObject.transform.parent);
            }*/
        }

        Managers.Character.GiveCoins(10);

        GetComponent<BoxCollider2D>().enabled = false;

    }
    public override void OnHit(int damage)
    {
        DamageFloater newDamageFloater = Instantiate(_damageFloaterPrefab);
        newDamageFloater.gameObject.SetActive(true);
        newDamageFloater.Initialize();
        newDamageFloater.transform.position = transform.position;
        newDamageFloater.GetComponent<TextMesh>().text = damage.ToString();
        Health -= damage;
        if (Health <= 0)
        {
            newDamageFloater.GetComponent<TextMesh>().text = (damage + Health).ToString();
            newDamageFloater.GetComponent<TextMesh>().color = new Color(1, 0, 0);
            Die();
            
        }
    }

    public void SetLevelVariables(int wave, int level, int bHP, int hpLvlUpgInc, int hpColUpgInc, int hpSizeUpgInc = 0)
    {
        Debug.Log("Slime created with wave " + wave);
        LevelCreated = level;
        WaveCreated = wave;
        HPLevelUpgradeIncrease = hpLvlUpgInc;
        HPColorUpgradeIncrease = hpColUpgInc;
        HPBase = bHP;
        int HPLevelAdjustment = ((LevelCreated / SlimeGeneric.slimeColorsList.Count)) * HPLevelUpgradeIncrease;
        HPBase += HPLevelAdjustment;
        animator = GetComponent<Animator>();
        //SlimesMovement = MovementType.Straight;
        if (SlimesMovement == MovementType.Pending)
        {
            if (wave % 3 == 1)
            {
                SlimesMovement = MovementType.Straight;
            }
            else if (wave % 3 == 2)
            {
                SlimesMovement = MovementType.Diagonal;
                xDir = 1 - (Random.Range(0, 2) * 2);
                //Debug.Log("Xdir set to " + xDir);
            }
            else if (wave % 3 == 0)
            {
                SlimesMovement = MovementType.ZigZag;
                xDir = Random.Range(-4, 4);
            }
        }

        speedAdjustment = 1;
        if (wave < 4)
        {
            transform.localScale = new Vector2(6, 6);
            animator.SetFloat("AnimSpeed", speedAdjustment + .4f);
        }
        else if (wave < 7)
        {
            transform.localScale = new Vector2(8, 8);
            animator.SetFloat("AnimSpeed", speedAdjustment + .2f);
            HPBase += hpSizeUpgInc;
        }
        else if (wave < 10)
        {
            transform.localScale = new Vector2(10, 10);
            animator.SetFloat("AnimSpeed", speedAdjustment);
            HPBase += hpSizeUpgInc * 2;
        }
        else if (wave < 13)
        {
            transform.localScale = new Vector2(12, 12);
            animator.SetFloat("AnimSpeed", speedAdjustment - .2f);
            HPBase += hpSizeUpgInc * 3;
        }
        //Health = HP;
        //speedAdjustment = speedAdj;
        
    }

    public void SetSlimeMovement(MovementType type)
    {
        SlimesMovement = type;
    }

    public void SetUpSpawning(int directionH, int directionV, Vector2 position, SlimeColor color)
    {
        
        InitializeVariables();
        /*if (slimeColorsList.IndexOf(color) < 4)
        {
            SlimesMovement = MovementType.Straight;
        } else if (slimeColorsList.IndexOf(color) < 8)
        {
            SlimesMovement = MovementType.Diagonal;
            xDir = 1 - (Random.Range(0, 2) * 2);
            Debug.Log("Xdir set to " + xDir);
        } else if (slimeColorsList.IndexOf(color) < 12)
        {
            SlimesMovement = MovementType.ZigZag;
            xDir = Random.Range(-4, 4);
        }*/
        DirectionH = directionH;
        DirectionV = directionV;
        SlimesColor = color;
        //animator = GetComponent<Animator>();
        //transform.localScale = new Vector2(6, 6);
        switch (color) {
            case SlimeColor.Green:
                resourcePathSlimeName = "slime_green";
                //animator.runtimeAnimatorController = _greenSlimeAnim;
                //animator.SetFloat("AnimSpeed", .4f + speedAdjustment);
                //Health = 10;
                break;
            case SlimeColor.Pink:
                //animator.runtimeAnimatorController = _pinkSlimeAnim;
                resourcePathSlimeName = "slime_pink";
                //animator.SetFloat("AnimSpeed", .2f + speedAdjustment);
                HPBase += HPColorUpgradeIncrease;
                //transform.localScale = new Vector2(8, 8);
               // Health = 20;
                break;
            case SlimeColor.Blue:
                resourcePathSlimeName = "slime_blue";
                //animator.runtimeAnimatorController = _blueSlimeAnim;
                //animator.SetFloat("AnimSpeed", speedAdjustment);
                //transform.localScale = new Vector2(10, 10);
                HPBase += HPColorUpgradeIncrease * 2;
                // Health = 30;
                break;
            case SlimeColor.Yellow:
                resourcePathSlimeName = "slime_4yellow";
                //animator.runtimeAnimatorController = _greenSlimeAnim;
                //animator.SetFloat("AnimSpeed",  speedAdjustment - .2f);
                //transform.localScale = new Vector2(12, 12);
                HPBase += HPColorUpgradeIncrease * 3;
                //Health = 10;
                break;
            case SlimeColor.Fifth:
                resourcePathSlimeName = "slime_5purple";
                //animator.runtimeAnimatorController = _pinkSlimeAnim;
                //animator.SetFloat("AnimSpeed", .4f + speedAdjustment);
                //transform.localScale = new Vector2(6, 6);
                HPBase += HPColorUpgradeIncrease * 4;
                // Health = 20;
                break;
            case SlimeColor.Sixth:
                resourcePathSlimeName = "slime_6orange";
                //animator.runtimeAnimatorController = _blueSlimeAnim;
                //animator.SetFloat("AnimSpeed", .2f + speedAdjustment);
                //transform.localScale = new Vector2(8, 8);
                HPBase += HPColorUpgradeIncrease * 5;
                // Health = 30;
                break;
            case SlimeColor.Seventh:
                resourcePathSlimeName = "slime_7grey";
                //animator.runtimeAnimatorController = _blueSlimeAnim;
                //animator.SetFloat("AnimSpeed", speedAdjustment);
                //transform.localScale = new Vector2(10, 10);
                HPBase += HPColorUpgradeIncrease * 6;
                // Health = 10;
                break;
            case SlimeColor.Eighth:
                resourcePathSlimeName = "slime_8forest";
                //animator.runtimeAnimatorController = _blueSlimeAnim;
                //animator.SetFloat("AnimSpeed", speedAdjustment - .2f);
                //transform.localScale = new Vector2(12, 12);
                HPBase += HPColorUpgradeIncrease * 7;
                // Health = 20;
                break;
            case SlimeColor.Ninth:
                resourcePathSlimeName = "slime_9red";
                //animator.runtimeAnimatorController = _blueSlimeAnim;
                //animator.SetFloat("AnimSpeed", .4f + speedAdjustment);
                //transform.localScale = new Vector2(6, 6);
                HPBase += HPColorUpgradeIncrease * 8;
                // Health = 30;
                break;
            case SlimeColor.Tenth:
                resourcePathSlimeName = "slime_10blue";
                //animator.runtimeAnimatorController = _blueSlimeAnim;
                //animator.SetFloat("AnimSpeed", .2f + speedAdjustment);
                //transform.localScale = new Vector2(8, 8);
                HPBase += HPColorUpgradeIncrease * 9;
                // Health = 10;
                break;
            case SlimeColor.Eleventh:
                resourcePathSlimeName = "slime_11brown";
                //animator.runtimeAnimatorController = _blueSlimeAnim;
                //animator.SetFloat("AnimSpeed", speedAdjustment);
                //transform.localScale = new Vector2(10, 10);
                HPBase += HPColorUpgradeIncrease * 10;
                // Health = 20;
                break;
            case SlimeColor.Twlefth:
                resourcePathSlimeName = "slime_12black";
                //animator.runtimeAnimatorController = _blueSlimeAnim;
                //animator.SetFloat("AnimSpeed", speedAdjustment - .2f);
                //transform.localScale = new Vector2(12, 12);
                HPBase += HPColorUpgradeIncrease * 11;
                //Health = 30;
                break;
        }
        GetComponent<ReskinAnimation>().UpdateSpritePath("Slimes/" + resourcePathSlimeName + "/left");
        Spawning = true;
        
        transform.position = position;
        Health = HPBase;
    }
}

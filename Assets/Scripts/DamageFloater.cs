﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageFloater : MonoBehaviour {
    float YVelocity = 2;
    float XVelocity = 1;
    float TotalDistance = 0;
	// Use this for initialization
	void Start () {
		
	}

    public void Initialize()
    {
        XVelocity = Random.Range(-2f, 2f);
    }

    // Update is called once per frame
    void Update () {
        transform.position += new Vector3(XVelocity * Time.deltaTime, YVelocity * Time.deltaTime, 0);
        TotalDistance += YVelocity * Time.deltaTime;

        if (TotalDistance > 2)
        {
            Destroy(gameObject);
        }
	}
}

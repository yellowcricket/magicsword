﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordAttack : MonoBehaviour {
    [SerializeField] Sprite _chargedSword;
    float velocity = -10;
    bool hitSomething = false;
    int damageMultiplier;
    int SwordHealth;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.localPosition -= new Vector3(0, velocity * Time.deltaTime, 0);

        if (transform.position.y > 13)
        {
            gameObject.SetActive(false);
        }
    }

    public void SetDamageMultiplier(float mult)
    {
        SwordHealth = (int)(Managers.Character.GetSwordAttackDamage() * mult);
        hitSomething = false;
        if (mult >= 2)
        {
            GetComponent<SpriteRenderer>().sprite = _chargedSword;
            GetComponent<BoxCollider2D>().size = new Vector2(.27f, .5f);
            GetComponent<BoxCollider2D>().offset = new Vector2(.01f, -.01f);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("something collided");
        if (collision.collider.GetComponent<Enemy>() != null)
        {
            if (!hitSomething)
            {

                //Debug.Log("green hit");
                int enemyHealth = collision.collider.GetComponent<Enemy>().Health;
                collision.collider.GetComponent<Enemy>().OnHit(SwordHealth);
                SwordHealth -= enemyHealth;
                if (SwordHealth <= 0)
                {
                    hitSomething = true;
                    //Destroy(gameObject);
                    gameObject.SetActive(false);
                }
            }
        }
        else if (collision.collider.GetComponent<Mushroom>() != null)
        {
            Destroy(collision.collider.gameObject);
            gameObject.SetActive(false);
            hitSomething = true;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeExplosion : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("something hit grenade");
        if (collision.GetComponent<Enemy>() != null)
        {
            int damage = Managers.Character.GetGrenadeDamage();
            collision.GetComponent<Enemy>().OnHit(damage);
            GetComponent<BoxCollider2D>().enabled = false;
        }

    }

}

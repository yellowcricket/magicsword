﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MissionManager : MonoBehaviour, IGameManager {
    public ManagerStatus status { get; private set; }

    public enum GameMode
    {
        Normal,
        Challenges,
        Quick
    }

    public GameMode gameMode;

    public int curLevel { get; set; }
    public int curWave { get; set; }

    public int waveTime = 10;

    public int HighScore { get; set; }
    public int HighScoreStreak { get; set; }


    private void Awake()
    {

    }
    // Update is called once per frame
    void Update () {
		
	}

    void IGameManager.Startup()
    {
        Debug.Log("Mission Manager started");
        curLevel = 1;//only used for testing purposes


        gameMode = GameMode.Normal;
        status = ManagerStatus.Started;
        
        
    }

    public void StartGame()
    {

            //curLevel = 1;
            string levelName = "GameLevel";
            SceneManager.LoadScene(levelName);

    }



    public void IncementWave() {
        curWave++;
        if (curWave == SlimeGeneric.slimeColorsList.Count + 1)
        {
            curWave = 1;
            curLevel++;//new level?
        }
        Managers.Data.SaveGamestate();
    }

    public void GoToNext()
    {
        //if(curLevel < maxLevel)
        //{
           // curLevel++;
            //if (curLevel > playerLevelUnlocked)
            //{
                //playerLevelUnlocked = curLevel;
            //}
            string levelName = "GameLevel";
            SceneManager.LoadScene(levelName);
        //} else
        //{
        //    Debug.Log("Last level");
        //}
    }

    public void RestartLevel()
    {

        string levelName = "GameLevel";
        SceneManager.LoadScene(levelName);

    }
}

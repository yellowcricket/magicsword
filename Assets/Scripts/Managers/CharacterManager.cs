﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterManager : MonoBehaviour, IGameManager
{

    public ManagerStatus status { get; private set; }

    public enum UpgradeTypes
    {
        Velocity,
        SwordPower
    }

    public int CoinsObtained { get; set; }
    public int GrenadesObtained { get; set; }

    public int SwordBaseDamage { get; set; }
    public int SwordDamagePerLevel { get; set; }
    public int SwordBaseCost { get; set; }
    public int SwordAdditionalCost { get; set; }

    public int GrenadeBaseDamage { get; set; }
    public int GrenadeDamagePerLevel { get; set; }
    public float GrenadeBaseRadius { get; set; }
    public float GrenadeRadiusPerLevel { get; set; }
    public int GrenadeBaseCost { get; set; }
    public int GrenadeAdditionalCost { get; set; }

    public const int HeroBaseVelocity = 4;
    public const int HeroVelocityPerLevel = 1;
    public const int HeroSpeedBaseCost = 10;
    public const int HeroSpeedAdditionalCost = 5;

    public int SwordAttackLevel { get; set; }
    public int GrenadeLevel { get; set; }
    public int HeroSpeedLevel { get; set; }

    public float MaximumYHeroPos = -6f;

    private Dictionary<UpgradeTypes, int> UpgradeLevels;

    public int GetGrenadeDamage()
    {
        return GrenadeBaseDamage + (GrenadeDamagePerLevel * (GrenadeLevel - 1));
    }

    public float GetGrenadeRadius()
    {
        return GrenadeBaseRadius + (GrenadeRadiusPerLevel * (GrenadeLevel - 1));
    }

    public int GetSwordAttackDamage()
    {
        return SwordBaseDamage + (SwordDamagePerLevel * (SwordAttackLevel - 1));
    }

    public int GetHeroSpeed()
    {
        return HeroBaseVelocity + (HeroVelocityPerLevel * (HeroSpeedLevel - 1));
    }

    public int GetSwordAttackCost()
    {
        return SwordBaseCost + (SwordAdditionalCost * (SwordAttackLevel - 1));
    }

    public int GetHeroSpeedCost()
    {
        return HeroSpeedBaseCost + (HeroSpeedAdditionalCost * (HeroSpeedLevel - 1));
    }

    public int GetGrenadeCost()
    {
        return GrenadeBaseCost + (GrenadeAdditionalCost  * (GrenadeLevel - 1));
    }

    public void UpgradeGrenadeLevel()
    {
        GiveCoins(-GetGrenadeCost());
        GrenadeLevel++;
    }

    public void UpgradeHeroSpeed()
    {
        
        GiveCoins(-GetHeroSpeedCost());
        HeroSpeedLevel++;
    }

    public void UpgradeSwordAttack()
    {
        GiveCoins(-GetSwordAttackCost());
        SwordAttackLevel++;
    }

    public void GiveCoins(int amount)
    {
        CoinsObtained += amount;
        Managers.Data.SaveGamestate();
    }

    void IGameManager.Startup()
    {
        status = ManagerStatus.Started;
        HeroSpeedLevel = 1;//temporary until data manager
        SwordAttackLevel = 1;
        GrenadeLevel = 1;
        Debug.Log("Character Manager started");
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using Facebook.Unity;
//using GameAnalyticsSDK;
//[RequireComponent(typeof(AudioManager))]

public class Managers : MonoBehaviour {

    public static Managers instance = null;
	//public static AudioManager Audio {get; private set;}
    public static MissionManager Mission { get; private set; }
    public static CharacterManager Character { get; private set; }
    public static AudioManager Audio { get; private set; }
    
    //public static PurchasesManager Purchases { get; private set; }
    public static DataManager Data { get; private set; }
    private List<IGameManager> _startSequence;

    void Awake() {

        if (instance == null)
        {

            //if not, set instance to this
            instance = this;
        }
        //If instance already exists and it's not this:
        else if (instance != this)
        { 
            Debug.Log("instance destroyed");
            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        Audio = GetComponent<AudioManager>();
        Mission = GetComponent<MissionManager>();
        Character = GetComponent<CharacterManager>();
        //Purchases = GetComponent<PurchasesManager>();
        Data = GetComponent<DataManager>();

        _startSequence = new List<IGameManager>();
		_startSequence.Add(Audio);
        _startSequence.Add(Mission);
        _startSequence.Add(Character);
        //_startSequence.Add(Purchases);
        _startSequence.Add(Data);

        StartCoroutine(StartupManagers());
	}
    private void Start()
    {
        //FB.Init();
        //GameAnalytics.Initialize();
    }

    private IEnumerator StartupManagers() {
		
		foreach (IGameManager manager in _startSequence) {
			manager.Startup();
		}

		yield return null;

		int numModules = _startSequence.Count;
		int numReady = 0;
		
		while (numReady < numModules) {
			int lastReady = numReady;
			numReady = 0;
			
			foreach (IGameManager manager in _startSequence) {
				if (manager.status == ManagerStatus.Started) {
					numReady++;
				}
			}

            if (numReady > lastReady)
            {
                Debug.Log("Progress: " + numReady + "/" + numModules);
                //Messenger<int, int>.Broadcast(StartupEvent.MANAGERS_PROGRESS, numReady, numModules);
            }
           
            yield return null;
		}
		
		Debug.Log("All managers started up");
        //Messenger.Broadcast(StartupEvent.MANAGERS_STARTED);
	}
}

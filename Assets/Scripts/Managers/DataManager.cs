﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class DataManager : MonoBehaviour, IGameManager
{

    public ManagerStatus status { get; private set; }

    private string _filename;


    public void Startup()
    {
        _filename = Path.Combine(Application.persistentDataPath, "gamev3.dat");
        LoadGameState();
        status = ManagerStatus.Started;
        Debug.Log("Data Manager started");
    }

    // Use this for initialization
    public void SaveGamestate()
    {
        Debug.Log("game saved");
        Dictionary<string, object> gamestate = new Dictionary<string, object>();
        gamestate.Add("levelunlocked", Managers.Mission.curLevel);
        gamestate.Add("waveunlocked", Managers.Mission.curWave);
        gamestate.Add("coins", Managers.Character.CoinsObtained);
        gamestate.Add("grenadelevel", Managers.Character.GrenadeLevel);
        gamestate.Add("grenadesobtained", Managers.Character.GrenadesObtained);
        gamestate.Add("attacklevel", Managers.Character.SwordAttackLevel);

        FileStream stream = File.Create(_filename);
        BinaryFormatter formatter = new BinaryFormatter();
        formatter.Serialize(stream, gamestate);
        stream.Close();

    }

    public void DeleteGamestate()
    {

        File.Delete(_filename);

        #if UNITY_EDITOR
            UnityEditor.AssetDatabase.Refresh();
        #endif
    }

    public bool GameStateExists() {
        return File.Exists(_filename);
    }

    public void LoadGameState()
    {
        
        Managers.Mission.curLevel = 1;
        Managers.Mission.curWave = 1;
        Managers.Character.GrenadeLevel = 1;
        Managers.Character.GrenadesObtained = 5;
        Managers.Character.SwordAttackLevel= 1;
        Managers.Character.CoinsObtained = -1;
        if (!File.Exists(_filename))
        {
            /*gamestate.Add("levelunlocked", Managers.Mission.curLevel);
            gamestate.Add("waveunlocks", Managers.Mission.curWave);
            gamestate.Add("coins", Managers.Character.CoinsObtained);
            gamestate.Add("grenadelevel", Managers.Character.GrenadeLevel);
            gamestate.Add("attacklevel", Managers.Character.SwordAttackLevel);*/

            return;
        }

        Debug.Log("Loading save...");
        Debug.Log(" file found under " + _filename);
        Dictionary<string, object> gamestate;

        BinaryFormatter formatter = new BinaryFormatter();
        FileStream stream = File.Open(_filename, FileMode.Open);
        gamestate = formatter.Deserialize(stream) as Dictionary<string, object>;
        stream.Close();


        if (gamestate.ContainsKey("levelunlocked"))
        {
            Managers.Mission.curLevel = (int)gamestate["levelunlocked"];

        }

        if (gamestate.ContainsKey("waveunlocked"))
        {
            Debug.Log("Wave loaded");
            Managers.Mission.curWave = (int)gamestate["waveunlocked"];

        }
        else
        {
            Debug.Log("Wave not loaded");
        }

        if (gamestate.ContainsKey("grenadelevel"))
        {
            Managers.Character.GrenadeLevel = (int)gamestate["grenadelevel"];

        }

        if (gamestate.ContainsKey("grenadesobtained")) {
            Managers.Character.GrenadesObtained = (int)gamestate["grenadesobtained"];
        }

        if (gamestate.ContainsKey("attacklevel"))
        {
            Managers.Character.SwordAttackLevel = (int)gamestate["attacklevel"];

        }

        if (gamestate.ContainsKey("coins"))
        {
            Managers.Character.CoinsObtained = (int)gamestate["coins"];

        }
    }

    public static Transform FindDeepChild(Transform aParent, string aName)
    {
        foreach (Transform child in aParent)
        {
            if (child.name == aName)
                return child;
            var result = FindDeepChild(child, aName);
            if (result != null)
                return result;
        }
        return null;
    }
}

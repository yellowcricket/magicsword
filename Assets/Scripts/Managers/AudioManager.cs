﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour, IGameManager{
    public ManagerStatus status { get; private set; }
    [SerializeField] AudioSource musicSource;
    [SerializeField] AudioSource soundSource;

    [SerializeField] private string levelBGMusic;

    [SerializeField] private AudioClip slimeDie;

    public bool MusicOn { get; set; }
    public bool SoundOn { get; set; }

    public void Startup()
    {
        //dogWhine1 = Resources.Load("Sound/dogwhine1") as AudioClip;
        status = ManagerStatus.Started;
        soundSource.volume = 1f;
    }

    void Awake()
    {
        MusicOn = true;
        SoundOn = true;
    }

    public void PlayLevelMusic(int song)
    {
        musicSource.volume = .2f;
        musicSource.loop = true;

        AudioClip audioclip = Resources.Load("Music/" + levelBGMusic) as AudioClip;
        PlayMusic(audioclip);

    }


    public void PauseMusic() {
        musicSource.Pause();
    }
    public void ContinueMusic() {
        musicSource.UnPause();
    }

    public void PlayMusic (AudioClip clip)
    {
        if (clip != null && MusicOn)
        {
            musicSource.clip = clip;
            musicSource.Play();
        }
    }

    //sounds

    public void PlaySlimeDie()
    {
        PlaySound(slimeDie);
    }

    //public IEnumerator ResetCoinPlayed()
    //{
    //    yield return new WaitForSeconds(.15f);
    //    CoinPlayed = false;
    //}

    public void PlaySound(AudioClip clip)
    {
        if (clip != null && SoundOn)
        {
            //musicSource.clip = clip;
            soundSource.volume = .5f;
            soundSource.PlayOneShot(clip, 1f);
        }
    }

    public void StopMusic()
    {
        musicSource.Stop();
    }
}

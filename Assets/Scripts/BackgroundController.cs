﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackgroundController : MonoBehaviour {
    [SerializeField] List<Sprite> _bgSprites;
    [SerializeField] SpriteRenderer _backgroundBack;
    [SerializeField] SpriteRenderer _backgroundFront;
    [SerializeField] Transform _chargeBarTop;
    [SerializeField] Transform _chargeBarContainer;
    [SerializeField] Transform _enemyHolder;
    bool transitioning = false;

    public Vector2 screenScale { get; private set; }
    // Use this for initialization
    void Start () {
        ResizeSpriteToScreen();
    }

    void ResizeSpriteToScreen()
    {

        transform.localScale = new Vector3(1, 1, 1);

        var width = _backgroundBack.sprite.bounds.size.x;
        var height = _backgroundBack.sprite.bounds.size.y;

        var worldScreenHeight = Camera.main.orthographicSize * 2.0;
        var worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

        screenScale = new Vector2((float)worldScreenWidth / width, (float)worldScreenHeight / height);
        _backgroundBack.transform.localScale = screenScale;
        _backgroundFront.transform.localScale = screenScale;
        _chargeBarContainer.transform.localScale = screenScale;
        _enemyHolder.transform.localScale = screenScale;
        MonstersController.ChargeBarCenterX = 0;
        MonstersController.ChargeBarCenterY = _chargeBarTop.GetComponent<Image>().sprite.bounds.size.y / 2
            + _chargeBarTop.position.y;

        Managers.Character.MaximumYHeroPos *= screenScale.y;
    }

    // Update is called once per frame
    void Update () {
        if (transitioning)
        {
            float newAlpha = _backgroundFront.color.a - 1f * Time.deltaTime;
            if (newAlpha < 0) {
                newAlpha = 0;
                transitioning = false;
            }
            _backgroundFront.color = new Color(1, 1, 1, newAlpha);
            
        }
        //_backgroundFront.transform.localScale = new Vector2(1, Screen.height /918.0f);
        //_backgroundBack.transform.localScale = new Vector2(1, Screen.height / 918.0f);
    }

    public void TransitionBackground()
    {
        
        int backgroundToTransitionTo = (Managers.Mission.curLevel - 1)  % _bgSprites.Count;
        Debug.Log("transitioning to background " + backgroundToTransitionTo.ToString());
        transitioning = true;
        _backgroundFront.sprite = _backgroundBack.sprite;
        _backgroundFront.color = new Color(1, 1, 1, 1);
        _backgroundBack.sprite = _bgSprites[backgroundToTransitionTo];
    }

    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeGreen : Enemy {
    Animator animator;

    bool Spawning = false;
    int Direction = 0;
    // Use this for initialization
    void Start () {
        animator = GetComponent<Animator>();
        //GetComponent<Animator>().state
    }
	
	// Update is called once per frame
	void Update () {

        if (animator.GetBool("FinishedInitialJump"))
        {
            Spawning = false;
        }

        if (Spawning)
        {
            transform.localPosition -= new Vector3(Direction * Time.deltaTime, 0, 0);
        }
        else if (!animator.GetBool("Grounded") && !animator.GetBool("Dying"))
        {
            transform.localPosition -= new Vector3(0, 1 * Time.deltaTime, 0) ;
        }
	}

    public void Die()
    {
        animator.SetBool("Dying", true);
        GetComponent<BoxCollider2D>().enabled = false;
    }
    public override void OnHit(int damage)
    {
        Health -= damage;
        if (Health < 0)
        {
            Die();
        }
    }


    public void SetUpSpawning(int direction, Vector2 position)
    {
        Direction = direction;

        Spawning = true;
        transform.localScale = new Vector2(5, 5);
        transform.position = position;
        animator = GetComponent<Animator>();
        animator.SetFloat("AnimSpeed", 1.2f);
    }

}

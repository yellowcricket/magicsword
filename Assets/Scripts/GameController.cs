﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GameController : MonoBehaviour {
    [SerializeField] Hero _hero;
    public List<SpriteRenderer> _chargeBar;
    public Image _chargeBarTop;
    public Image _chargeBarBottom;
    [SerializeField] Image _staminaBar;
    [SerializeField] Camera _camera;
    [SerializeField] GameObject _upgradesUI;
    [SerializeField] GameObject _gameOverUI;
    [SerializeField] Transform _monstersHolder;
    [SerializeField] Transform _mushroomsHolder;

    [SerializeField] Text _coinsTextMesh;
    [SerializeField] Text _grenadesTextMesh;

    [SerializeField] int _heroVelocity;
    [SerializeField] int _heroBaseAttackDamage;
    [SerializeField] int _heroAttackDamageBonusPerUpgrade;
    [SerializeField] int _heroAttackDamageBaseCost;
    [SerializeField] int _heroAttackDamageCostIncreasePerUpgrade;
    [SerializeField] float _heroAttackTimeDelay = 0;
    [SerializeField] float _heroAttackChargeTime = 2.5f;
    [SerializeField] float _heroMaxStamina = 3;
    [SerializeField] float _heroStaminaChargeTime = 1.5f;
    [SerializeField] int _heroGrenadeDamage;
    [SerializeField] int _heroGrenadeDamageBonusPerUpgrade;
    [SerializeField] int _heroGrenadeDamageBaseCost;
    [SerializeField] int _heroGrenadeDamageCostIncreasePerUpgrade;
    [SerializeField] float _heroGrenadeBaseBlastRadius = 10;
    [SerializeField] float _heroGrenadeBlastRadiusIncreasePerUpgrade = .1f;

    [SerializeField] int _usersStartingCoins = 1000;
    [SerializeField] bool clearSave = false;

    const float baseScreenHeight = 1024f;
    const float baseScreenWidth = 768f;
    public enum CurrentGameState
    {
        PreStart,
        Playing,
        Victory,
        Defeat,
    }
    private CurrentGameState _curGameState;

    //powers variables
   // private float ChargeStartTime;

   // private bool Charging = false;
    private float yTouchDown;
	// Use this for initialization
	void Start () {
        if (clearSave)
        {
            Managers.Data.DeleteGamestate();
            Managers.Data.LoadGameState();
        }
        _curGameState = CurrentGameState.PreStart;
        float upscaledLiveWidth = Screen.width * (baseScreenHeight / Screen.height);
        _camera.orthographicSize = (baseScreenWidth / upscaledLiveWidth) * 10.24f;

        _hero.CurrentVelocity = _heroVelocity;
        Managers.Character.SwordBaseDamage = _heroBaseAttackDamage;
        Managers.Character.SwordDamagePerLevel = _heroAttackDamageBonusPerUpgrade;
        Managers.Character.SwordBaseCost = _heroAttackDamageBaseCost;
        Managers.Character.SwordAdditionalCost = _heroAttackDamageCostIncreasePerUpgrade;
        _hero.SwordAttackTimeDelay = _heroAttackTimeDelay;

        Managers.Character.GrenadeBaseDamage = _heroGrenadeDamage;
        Managers.Character.GrenadeDamagePerLevel = _heroGrenadeDamageBonusPerUpgrade;
        Managers.Character.GrenadeBaseCost = _heroGrenadeDamageBaseCost;
        Managers.Character.GrenadeAdditionalCost = _heroGrenadeDamageCostIncreasePerUpgrade;
        Managers.Character.GrenadeBaseRadius = _heroGrenadeBaseBlastRadius;
        Managers.Character.GrenadeRadiusPerLevel = _heroGrenadeBlastRadiusIncreasePerUpgrade;
        _hero.StaminaChargeTime = _heroStaminaChargeTime;
        _hero.SwordMaxStamina = _heroMaxStamina;
        _hero.SwordStamina = _heroMaxStamina;
        _hero.HeroAttackChargeTime = _heroAttackChargeTime;
        if (Managers.Character.CoinsObtained < 0)
        {
            Managers.Character.CoinsObtained = _usersStartingCoins;
        }
    }

	
	// Update is called once per frame
	void FixedUpdate () {

        //if ((!EventSystem.current.IsPointerOverGameObject(0) && !EventSystem.current.IsPointerOverGameObject())
        //          || EventSystem.current.currentSelectedGameObject == null)
        //{
        

            //}
    }


    private bool IsPointerOverUIObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }

    private void Update()
    {
        if (_curGameState == CurrentGameState.PreStart && Input.GetMouseButtonUp(0))
        {
            if ((!EventSystem.current.IsPointerOverGameObject(0) && !IsPointerOverUIObject()) || EventSystem.current.currentSelectedGameObject == null)
            {
                _monstersHolder.gameObject.SetActive(true);
                _curGameState = CurrentGameState.Playing;
                Managers.Audio.PlayLevelMusic(1);
                _hero.GetComponent<Hero>().enabled = true;
                _hero.GetComponent<Hero>().Initialize();
                _upgradesUI.SetActive(false);
                GetComponent<MonstersController>().enabled = true;
                GetComponent<MonstersController>().Initialize();
            }
        }
        else if (_curGameState == CurrentGameState.Defeat && Input.GetMouseButtonUp(0))
        {
            ClearEnemies();
            _curGameState = CurrentGameState.PreStart;
            _upgradesUI.SetActive(true);
            _gameOverUI.SetActive(false);
        }
        else if (_curGameState == CurrentGameState.Playing)
        {


            float fillPercentage = _hero.SwordStamina / _hero.SwordMaxStamina;
            _staminaBar.fillAmount = fillPercentage;
            if (MonsterBreached())
            {
                _curGameState = CurrentGameState.Defeat;
                _monstersHolder.gameObject.SetActive(false);
                _gameOverUI.SetActive(true);
                return;
            }

            if (MushroomBreached())
            {
                _hero.ActivateMushroomPowerup();
            }
            if (Input.GetMouseButtonDown(0))
            {
                //yTouchDown = Input.mousePosition.y;
                yTouchDown = _camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0)).y;
                _hero.ChargeStartTime = Time.time;
                
                //Charging = true;
                _hero.SetChargeAnimation(true);
                //Debug.Log("Down" + Input.mousePosition.y);
                //start charge, or detect swipe
            }
            if (Input.GetMouseButtonUp(0) && _hero.CurrentlyCharging)
            {
                //_hero.SetChargeAnimation(false);
                //Debug.Log("World space is " + _camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0)));
                float yTouchUp = _camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0)).y;
                //if (Input.mousePosition.y - yTouchDown > 50 && Time.time - ChargeStartTime < 1) {
                if (yTouchUp - yTouchDown > 1 && Time.time - _hero.ChargeStartTime < 1)
                {
                    //throw bomb
                    float pixelDistance = Input.mousePosition.y - yTouchDown;
                    float distancePercentage = pixelDistance / Screen.height;

                    //_hero.DoGrenadeAttack((yTouchUp - yTouchDown) * 2f);
                    _hero.DoGrenadeAttack(0);
                }
                else
                {
                    /*float ChargeTimeMultiplier = 5 / _heroAttackChargeTime;
                    float damageMultiplier = ((Time.time - ChargeStartTime) * ChargeTimeMultiplier) + 1;
                    if (damageMultiplier > 5)
                    {
                        damageMultiplier = 5;
                    }
                    Debug.Log("Multiplier is " + damageMultiplier);*/
                    _hero.DoSwordAttack();

                }
                _hero.SetChargeAnimation(false);
                _hero.SetAttackAnimation(true);
                //Debug.Log("Up" + Input.mousePosition.y);
            }

            _coinsTextMesh.text = Managers.Character.CoinsObtained.ToString();
            _grenadesTextMesh.text = Managers.Character.GrenadesObtained.ToString();
        }
        float ChargeTimeMult = 5 / _heroAttackChargeTime;
        float ChargeTime = (Time.time - _hero.ChargeStartTime) * ChargeTimeMult;
        if (_hero.CurrentlyCharging)
        {
            if (ChargeTime > 4)
            {
                _chargeBarTop.fillAmount = 1;
            }
            else
            {
                _chargeBarTop.fillAmount = ChargeTime / 4;
            }
        }
        else
        {
            _chargeBarTop.fillAmount = .028f;
        }
        /*_chargeBar[4].enabled = false;
        _chargeBar[3].enabled = false;
        _chargeBar[2].enabled = false;
        _chargeBar[1].enabled = false;
        _chargeBar[0].enabled = false;
        if (_hero.CurrentlyCharging)
        {
            if (ChargeTime > 4)
            {
                _chargeBar[4].enabled = true;
            }
            else if (ChargeTime > 3)
            {
                _chargeBar[3].enabled = true;
            }
            else if (ChargeTime > 2)
            {
                _chargeBar[2].enabled = true;
            }
            else if (ChargeTime > 1)
            {
                _chargeBar[1].enabled = true;
            }
            else
            {
                _chargeBar[0].enabled = true;
            }
        }
        else
        {
            _chargeBar[0].enabled = true;
        }*/


        if (_curGameState == CurrentGameState.PreStart)
        {
        }
    }

    private bool MonsterBreached()
    {
        for (int i = 0; i < _monstersHolder.childCount; i++)
        {
            Transform monster = _monstersHolder.GetChild(i);
            if (monster.GetComponent<DamageFloater>())
            {
                continue;
            }

            if (monster.transform.position.y + monster.GetComponent<SpriteRenderer>().sprite.bounds.size.y//if the bottom of slime is below midpoint of charge bar
                < _chargeBarTop.transform.position.y + (_chargeBarTop.sprite.bounds.size.y)
                && !monster.GetComponent<SlimeGeneric>().Dying) {
                return true;
            }
        }
        return false;
    }

    private bool MushroomBreached()
    {
        for (int i = 0; i < _mushroomsHolder.childCount; i++)
        {
            Transform mushroom = _mushroomsHolder.GetChild(i);
            if (mushroom.GetComponent<Mushroom>().MushroomObtained) {
                mushroom.gameObject.SetActive(false);
                Destroy(mushroom.gameObject);
                return true;
            }
        }
            return false;
    }

    private void ClearEnemies()
    {
        for (int i = 0; i < _monstersHolder.childCount; i++)
        {
            Transform monster = _monstersHolder.GetChild(i);
            if (monster.name.Contains("Clone"))
            {
                Destroy(monster.gameObject);
            }
        }
    }
}

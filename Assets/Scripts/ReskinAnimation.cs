﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ReskinAnimation : MonoBehaviour {

    string spritePath;
    public void UpdateSpritePath(string path) {
        spritePath = path;
    }

    void LateUpdate () {
        replaceImages();
	}

    public void replaceImages()
    {
        var subSprites = Resources.LoadAll<Sprite>(spritePath);

        foreach (var renderer in GetComponentsInChildren<SpriteRenderer>())
        {
            string spriteName = renderer.sprite.name;
            var newSprite = Array.Find(subSprites, item => item.name == spriteName);

            if (newSprite)
            {
                renderer.sprite = newSprite;
            }
        }
    }
}

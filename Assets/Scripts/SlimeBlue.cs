﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeBlue : Enemy {
    [SerializeField] SlimeRed _redSlimePrefab;

    Animator animator;
    bool IsBig = true;
    bool Spawning = false;
    int Direction = 0;
    
    // Use this for initialization
    void Start()
    {
        animator = GetComponent<Animator>();
        //GetComponent<Animator>().state
    }

    // Update is called once per frame
    void Update()
    {
        if (animator.GetBool("FinishedInitialJump"))
        {
            Spawning = false;
        }
        if (Spawning)
        {
            transform.localPosition -= new Vector3(Direction * Time.deltaTime, 0, 0);
        }
        else if (!animator.GetBool("Grounded") && !animator.GetBool("Dying"))
        {
            transform.localPosition -= new Vector3(0, 1 * Time.deltaTime, 0);
        }
    }

    public void Die()
    {
        animator.SetBool("Dying", true);
        
        //Generate Two new smaller ones
        //if (IsBig)
        //{
            SlimeRed babySlime1 = Instantiate(_redSlimePrefab).GetComponent<SlimeRed>();
            SlimeRed babySlime2 = Instantiate(_redSlimePrefab).GetComponent<SlimeRed>();
            babySlime1.gameObject.SetActive(true);
            babySlime2.gameObject.SetActive(true);
        babySlime1.SetUpSpawning(-2, 0, transform.position);
            babySlime2.SetUpSpawning(2, 0, transform.position);
            
        //}
        GetComponent<BoxCollider2D>().enabled = false;
    }
    public override void OnHit(int damage)
    {
        Health -= damage;
        if (Health < 0)
        {
            Die();
        }
    }

    public void SetUpSpawning(bool big, int direction, Vector2 position)
    {
        Direction = direction;
             
        Spawning = true;
        IsBig = big;
        transform.localScale = new Vector2(5, 5);
        animator = GetComponent<Animator>();
        animator.SetFloat("AnimSpeed", 1.2f);
    }
}

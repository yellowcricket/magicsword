﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hero : MonoBehaviour {
    [SerializeField] Camera _camera;
    [SerializeField] SwordAttack _swordPrefab;
    [SerializeField] GrenadeAttack _grenadePrefab;
    Animator animator;
    //movement variables
    private bool InputStarted = false;
    public float ArchivedPosition { get; set; }
    private float LastKnownPosition;
    private float InitialClickPosition;
    public const float PlayerRightBounds = 8f;
    public float CurrentVelocity { get; set; }

    public bool CurrentlyCharging { get; private set; }
    private bool CurrentlyAttacking = false;
    private float LastSwordAttackTime;
    public float SwordAttackTimeDelay { get; set; }
    public float SwordStamina { get; set; }
    public float SwordMaxStamina { get; set; }

    private int MushroomUpgradeLength = 10;
    private bool MushroomActive = false;
    private int HeroScale = 10;

    private float LastStaminaUpdateTime;
    public float StaminaChargeTime;

    public float ChargeStartTime;
    public float HeroAttackChargeTime { get; set; }

    // Use this for initialization
    void Start () {
        Initialize();
    }

    public void Initialize()
    {
        CurrentlyCharging = false; 
        ArchivedPosition = Screen.width / 2;
        LastKnownPosition = Screen.width / 2;
        animator = GetComponent<Animator>();
        LastStaminaUpdateTime = Time.time;

    }

    // Update is called once per frame
    void Update () {
        getDeviceInputForMovement();
        if (SwordStamina < SwordMaxStamina)
        {
            SwordStamina += (Time.time - LastStaminaUpdateTime) / StaminaChargeTime;

        }
        LastStaminaUpdateTime = Time.time;
        if (SwordStamina > SwordMaxStamina)
        {
            SwordStamina = SwordMaxStamina;
        }
    }

    public void DoSwordAttack()
    {

        float ChargeTimeMultiplier = 5 /HeroAttackChargeTime;
        float damageMultiplier = ((Time.time - ChargeStartTime) * ChargeTimeMultiplier) + 1;
        if (damageMultiplier > 5)
        {
            damageMultiplier = 5;
        }
        Debug.Log("Multiplier is " + damageMultiplier);

        if (Time.time >= LastSwordAttackTime + SwordAttackTimeDelay
            && SwordStamina >= 1)
        {
            SwordAttack newSwordAttack = Instantiate(_swordPrefab);
            newSwordAttack.transform.SetParent(_swordPrefab.transform.parent);
            newSwordAttack.gameObject.SetActive(true);
            newSwordAttack.SetDamageMultiplier(damageMultiplier);
            newSwordAttack.transform.position = transform.position;
            newSwordAttack.transform.localScale = new Vector2(10, 10);
            Physics2D.IgnoreCollision(newSwordAttack.GetComponent<BoxCollider2D>(), GetComponent<BoxCollider2D>());
            LastSwordAttackTime = Time.time;
            SwordStamina--;

        }
        /*if (!_swordPrefab.gameObject.activeSelf)
        {
            _swordPrefab.gameObject.SetActive(true);
            if (MushroomActive)
            {
                multiplier++;
            }
            _swordPrefab.SetDamageMultiplier(multiplier);
            _swordPrefab.transform.position = transform.position;
            _swordPrefab.transform.localScale = new Vector2(10, 10);
        }*/
    }

    public void DoGrenadeAttack(float distance)
    {
        if (Managers.Character.GrenadesObtained <= 0 && Managers.Character.CoinsObtained >= 100)
        {
            Managers.Character.GrenadesObtained += 5;
            Managers.Character.GiveCoins(-100);
        }
        if (Managers.Character.GrenadesObtained > 0)
        {

            Managers.Character.GrenadesObtained--;
            GrenadeAttack newGrenadeAttack = Instantiate(_grenadePrefab);
            newGrenadeAttack.gameObject.SetActive(true);
            newGrenadeAttack.SetDistanceToGo(distance);
            newGrenadeAttack.transform.position = new Vector2 (transform.position.x, transform.position.y + 1.5f);
            newGrenadeAttack.transform.localScale = new Vector2(2,2);
        }
    }

    public void SetChargeAnimation( bool on)
    {
        animator.SetBool("Charging", on);
        CurrentlyCharging = on;
    }

    public void SetAttackAnimation(bool on)
    {
        animator.SetBool("Attacking", on);
        CurrentlyAttacking = on;
    }

    public void ActivateMushroomPowerup()
    {
        StartCoroutine(DoMushroomPowerup());
    }

    private IEnumerator DoMushroomPowerup()
    {
        HeroScale = 12;
        MushroomActive = true;
        transform.localScale = new Vector3(HeroScale, HeroScale, 0);
        yield return new WaitForSeconds(MushroomUpgradeLength);
        MushroomActive = false;
        HeroScale = 10;
        transform.localScale = new Vector3(HeroScale, HeroScale, 0);
        yield return null;
    }

    private void getDeviceInputForMovement()
    {

        Vector3 curRelativePosition = new Vector3();
        bool inputObtained = false;
        if (Input.GetMouseButton(0))
        {
            Vector3 mousePosWorld = _camera.ScreenToWorldPoint(Input.mousePosition);
            float posX = mousePosWorld.x;
            float posY = mousePosWorld.y;

            if (posY > Managers.Character.MaximumYHeroPos)
            {
                posY = Managers.Character.MaximumYHeroPos;
            }

            transform.position = new Vector3(posX, posY, transform.position.z);

        }
        /*if (Input.GetMouseButton(0))
        {
            if (!InputStarted)
            {
                InputStarted = true;
                InitialClickPosition = Input.mousePosition.x;
                //Debug.Log("ARching position " + ArchivedPosition);
            }

            curRelativePosition = new Vector3(Input.mousePosition.x - InitialClickPosition + ArchivedPosition, Input.mousePosition.y, Input.mousePosition.z);
            inputObtained = true;


            LastKnownPosition = curRelativePosition.x;


        }
        else
        {
            ArchivedPosition = _camera.WorldToScreenPoint(transform.position).x;
            //ArchivedPosition = LastKnownPosition;//SP is percentage from left of screen
            inputObtained = false;
            InputStarted = false;

        }


        if (inputObtained)
        {
            float xPoint = curRelativePosition.x;
            xPoint -= (Screen.width / 2);
            //xPoint *= 3f; //get 300% of the coordinates so they don't have to put their finger all the way to the edge.
            xPoint += (Screen.width / 2);
            if (xPoint < 0)
            {
                xPoint = 0;
            }
            else if (xPoint > Screen.width)
            {
                xPoint = Screen.width;
            }
            float positionPercentage = xPoint / (float)Screen.width;

            float heroPositionToAimFor = (PlayerRightBounds * 2 * positionPercentage) - PlayerRightBounds;

            float newHeroPosition = transform.position.x;

            if (transform.position.x > heroPositionToAimFor + 1)
            {
                //newHeroPosition -= Managers.Character.GetHeroSpeed() * Time.deltaTime;
                newHeroPosition -= CurrentVelocity * Time.deltaTime;
                if (newHeroPosition < heroPositionToAimFor)
                {
                    newHeroPosition = heroPositionToAimFor;
                }
                animator.SetBool("Walking", true);
                //SetChargeAnimation(false);
                transform.localScale = new Vector3(HeroScale, HeroScale, 0);
            }
            else if (transform.position.x < heroPositionToAimFor - 1)
            {
                //newHeroPosition += Managers.Character.GetHeroSpeed() * Time.deltaTime;
                newHeroPosition += CurrentVelocity * Time.deltaTime;
                if (newHeroPosition > heroPositionToAimFor)
                {
                    newHeroPosition = heroPositionToAimFor;
                }
                animator.SetBool("Walking", true);
                //SetChargeAnimation(false);
                transform.localScale = new Vector3(-HeroScale, HeroScale, 0);
            }
            else
            {
                transform.localScale = new Vector3(HeroScale, HeroScale, 0);
                animator.SetBool("Walking", false);
            }
            transform.position = new Vector3(newHeroPosition, transform.position.y, transform.position.z);

        }
        else
        {
            //transform.localScale = new Vector3(10, 10, 0);
            animator.SetBool("Walking", false);
        }*/

    }
}

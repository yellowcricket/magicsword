﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeRed : Enemy {
    [SerializeField] SlimeGreen _greenSlimePrefab;

    Animator animator;
    /*bool DirectionNeedsUpdating = true;
    bool DirectionUpdated = false;*/
    float xDir = 0;

    //bool IsBig = true;
    bool Spawning = false;
    int DirectionH = 0;
    int DirectionV = 0;

    // Use this for initialization
    void Start()
    {
        animator = GetComponent<Animator>();
        //GetComponent<Animator>().state
    }

    // Update is called once per frame
    void Update()
    {
        
        /*if (DirectionNeedsUpdating)
        {
            DirectionNeedsUpdating = false;
            DirectionUpdated = true;
            xDir = Random.Range(-4, 4);
            if (transform.localPosition.x < -6)
            {
                xDir = Random.Range(0, 4);
            }
            else if (transform.localPosition.x > 6)
            {
                xDir = Random.Range(-4, 0);
            }
        }*/

        if (animator.GetBool("FinishedInitialJump"))
        {
            Spawning = false;
        }

        if (Spawning)
        {
            transform.localPosition -= new Vector3(DirectionH * Time.deltaTime, DirectionV * Time.deltaTime, 0);
        }
        else if (!animator.GetBool("Grounded") && !animator.GetBool("Dying"))
        {
            transform.localPosition -= new Vector3(-xDir * Time.deltaTime, 1 * Time.deltaTime, 0);
            //DirectionUpdated = false;
        }

        /*if (animator.GetBool("Grounded") && !DirectionUpdated)
        {
            DirectionNeedsUpdating = true;
        }*/
    }

    public void Die()
    {
        animator.SetBool("Dying", true);

        SlimeGreen babySlime1 = Instantiate(_greenSlimePrefab).GetComponent<SlimeGreen>();
        babySlime1.gameObject.SetActive(true);
        SlimeGreen babySlime2 = Instantiate(_greenSlimePrefab).GetComponent<SlimeGreen>();
        babySlime2.gameObject.SetActive(true);
        babySlime1.SetUpSpawning(-2, transform.position);
        babySlime2.SetUpSpawning(2, transform.position);

        GetComponent<BoxCollider2D>().enabled = false;
    }
    public override void OnHit(int damage)
    {
        Health -= damage;
        if (Health < 0)
        {
            Die();
        }
    }

    public void SetUpSpawning(int directionH, int directionV, Vector2 position)
    {
        DirectionH = directionH;
        DirectionV = directionV;

        Spawning = true;
        transform.localScale = new Vector2(10, 10);
        transform.position = position;
        animator = GetComponent<Animator>();
        animator.SetFloat("AnimSpeed", 1.2f);
    }

}
